# Navigator for LOS & LMS

## Configure local development for a Tenant

Install npm and bower modules using below commands:
```
<project_Folder_path> npm i -g bower grunt yarn
<project_Folder_path> bower install
<project_Folder_path> npm i  or <project_Folder_path> yarn install
```
After that create a bat file with below configuration, Make sure not to check-in this file by updating (.gitignore).

```bat
SET PORT=5000
SET NODE_ENV=development
SET CONFIGURATION_HOST=http://<config_service_endpoint>:7001/
SET SECURITY_HOST=http://<identity_service_endpoint>:7018
SET VERIFICATION_ENGINE_HOST=http://<verification_service_endpoint>:7020/
SET STATUS_MANAGEMENT_HOST=http://<status_management_service_endpoint>:7021/
SET APP_TOKEN=Bearer <tenant_token>
grunt develop --portal=<portal_Name> && node ./server/server.js
```

Create file using above configuration.

```
<project_Folder_path> launch.bat
```

This will run on http://localhost:5000/

## Building Docker image for Navigator 

`bash build_script.sh <tenant_name> <image_name> <tag_name>`