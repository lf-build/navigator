'use strict';

const fs = require('fs');
const Promise = require("bluebird");
const request = require('request');
const jwt = require("jsonwebtoken");

const CONFIG_HOST = process.env.CONFIGURATION_HOST;
const SECURITY_HOST = process.env.SECURITY_HOST;
const ROUTING_NUMBER_API = process.env.ROUTING_NUMBER_API;
const IFSC_API = process.env.IFSC_API;
const PORTAL = process.env.ALLOY_PORTAL;
const CONFIGURATION_EXECUTION_MODE = process.env.CONFIGURATION_EXECUTION_MODE;
const SECRET_KEY = process.env.SECRET_KEY;
const TENANT = process.env.TENANT_NAME;
var token = null;
var expiration = null;
Promise.promisifyAll(fs);

function addMinutes(date, m) {
    date.setTime(date.getTime() + (m * 60 * 1000));
    return date;
}

function getToken() {
    if (token !== null && expiration >= new Date()) {
        return "Bearer " + token;
    }
    if (SECRET_KEY === undefined) { return null }
    var key = Buffer.from(SECRET_KEY, 'base64');
    var currentDate = new Date();
    expiration = addMinutes(currentDate, 1);
    token = jwt.sign({ "tenant": TENANT, "sub": TENANT + "-" + PORTAL, "iss": "lendfoundry", "IsValid": true }, key, { expiresIn: 1 * 60 }); // expires in 24 hours
    return "Bearer " + token;
}

module.exports = function allRouters(app) {

    app.route('/request-token/').post((req, res) => {
        if (!req.headers['authorization']) {
            req.headers['authorization'] = getToken();
        }
        if (!req.headers["authorization"]) {
            res.send(401, "Unable to generate token");
        }
        let options = {
            url: SECURITY_HOST + req.originalUrl,
            headers: req.headers,
            json: true,
            body: req.body
        };
        request.post(options).pipe(res);
    });

    app.route('/login/').post((req, res) => {
        if (!req.headers['authorization']) {
            req.headers['authorization'] = getToken();
        }
        if (!req.headers["authorization"]) {
            res.send(401, "Unable to generate token");
        }
        let options = {
            url: SECURITY_HOST + req.originalUrl,
            headers: req.headers,
            json: true,
            body: req.body
        };
        request.post(options).pipe(res);
    });

    app.route('/reset-password/:token').post((req, res) => {
        if (!req.headers['authorization']) {
            req.headers['authorization'] = getToken();
        }
        if (!req.headers["authorization"]) {
            res.send(401, "Unable to generate token");
        }
        let options = {
            url: SECURITY_HOST + req.originalUrl,
            headers: req.headers,
            json: true,
            body: req.body
        };
        request.post(options).pipe(res);
    });

    app.route('/alloy/').get((req, res) => {
        if (!req.headers['authorization']) {
            req.headers['authorization'] = getToken();
        }
        if (!req.headers["authorization"]) {
            res.send(401, "Unable to generate token");
        }
        let options = {
            url: [CONFIG_HOST, "alloy"].join('/'),
            headers: req.headers
        };
        request.get(options).pipe(res);
    });

    app.route('/portal/').get((req, res) => {
        if (!req.headers['authorization']) {
            req.headers['authorization'] = getToken();
        }
        if (!req.headers["authorization"]) {
            res.send(401, "Unable to generate token");
        }
        let options = {
            url: [CONFIG_HOST, PORTAL].join('/'),
            headers: req.headers
        };
        request.get(options).pipe(res);
    });

    app.route('/tenant/').get((req, res) => {
        if (!req.headers['authorization']) {
            req.headers['authorization'] = getToken();
        }
        if (!req.headers["authorization"]) {
            res.send(401, "Unable to generate token");
        }
        let options = {
            url: [CONFIG_HOST, "tenant"].join('/'),
            headers: req.headers
        };
        request.get(options).pipe(res);
    });

    app.route('/configuration/:keyName(*)').get((req, res) => {
        let optionsForPortalConfiguration = {
            url: [CONFIG_HOST, PORTAL, 'configurationNames'].join('/'),
            headers: {
                "authorization": req.headers['authorization']
            },
            json: true
        };
        request.get(optionsForPortalConfiguration, function (err, response, body) {
            if (!response || response.statusCode !== 200 || !body) {
                res.send(401, "not allowed");
                return;
            } else {
                let items = body;
                if (!items) {
                    res.send(401, "not allowed");
                    return;
                } else if (items.indexOf(req.params.keyName) < 0) {
                    res.send(401, "not allowed");
                    return;
                } else {
                    let options = {
                        url: [CONFIG_HOST, req.params.keyName].join('/'),
                        headers: req.headers
                    };
                    request.get(options).pipe(res);
                }
            }
        });
    });

    app.route('/configuration/:keyName(*)').post((req, res) => {
        if (CONFIGURATION_EXECUTION_MODE !== "RW") {
            res.send(401, "not allowed");
            return;
        }

        let optionsForPortalConfiguration = {
            url: [CONFIG_HOST, PORTAL, 'configurationNames'].join('/'),
            headers: {
                "authorization": req.headers['authorization'],
            },
            json: true
        };
        request.get(optionsForPortalConfiguration, function (err, response, body) {
            if (!response || response.statusCode !== 200 || !body) {
                res.send(401, "not allowed");
                return;
            } else {
                let items = body;
                if (!items) {
                    res.send(401, "not allowed");
                    return;
                } else if (items.indexOf(req.params.keyName) < 0) {
                    res.send(401, "not allowed");
                    return;
                } else {
                    let options = {
                        url: [CONFIG_HOST, req.params.keyName].join('/'),
                        headers: req.headers,
                        json: true,
                        body: req.body
                    };
                    request.post(options).pipe(res);
                }
            }
        });
    });

    app.route('/logout/').get((req, res) => {
        let options = {
            url: SECURITY_HOST + req.originalUrl,
            headers: req.headers
        };
        request.get(options).pipe(res);
    });

    app.route('/is-expiring/').get((req, res) => {
        let options = {
            url: SECURITY_HOST + req.originalUrl,
            headers: req.headers
        };
        request.get(options).pipe(res);
    });

    app.route('/is-authorized/:path(*)').get((req, res) => {
        let options = {
            url: SECURITY_HOST + req.originalUrl,
            headers: req.headers
        };
        request.get(options).pipe(res);
    });

    app.route('/renew-token/').get((req, res) => {
        let options = {
            url: SECURITY_HOST + req.originalUrl,
            headers: req.headers
        };
        request.get(options).pipe(res);
    });

    app.route('/roles/:keyName(*)').get((req, res) => {
        let options = {
            url: SECURITY_HOST + req.originalUrl,
            headers: req.headers
        };
        request.get(options).pipe(res);
    });

    app.route('/check/').post((req, res) => {
        let options = {
            url: SECURITY_HOST + req.originalUrl,
            headers: req.headers,
            json: true,
            body: req.body
        };
        request.post(options).pipe(res);
    });

    app.route('/create-user/').put((req, res) => {
        let options = {
            url: SECURITY_HOST + "/",
            headers: req.headers,
            body: req.body,
            json: true
        };
        request.put(options).pipe(res);
    });

    app.route('/user-info/').get((req, res) => {
        let options = {
            url: SECURITY_HOST + "/",
            headers: req.headers
        };
        request.get(options).pipe(res);
    });

    app.route('/update/').put((req, res) => {
        let options = {
            url: SECURITY_HOST + req.originalUrl,
            headers: req.headers,
            body: req.body,
            json: true,
            method: 'PUT'
        };
        request.put(options).pipe(res);
    });

    app.route('/user/:id').get((req, res) => {
        let options = {
            url: SECURITY_HOST + req.originalUrl,
            headers: req.headers
        };
        request.get(options).pipe(res);
    });

    app.route('/user/:path(*)').put((req, res) => {
        let endpoint = "/" + req.params.path;
        let options = {
            url: SECURITY_HOST + endpoint,
            headers: req.headers,
            body: req.body,
            json: true
        };
        request.put(options).pipe(res);
    });

    app.route('/all/users/:path(*)').get((req, res) => {
        let options = {
            url: SECURITY_HOST + req.originalUrl,
            headers: req.headers
        };
        request.get(options).pipe(res);
    });

    app.route('/bank-name/:routingNumber').get((req, res) => {
        let options = {
            url: ROUTING_NUMBER_API + "name.json?rn=" + req.params.routingNumber
        };
        request.get(options).pipe(res);
    });

    app.route('/bank-details/:routingNumber').get((req, res) => {
        let options = {
            url: ROUTING_NUMBER_API + "data.json?rn=" + req.params.routingNumber
        };
        request.get(options).pipe(res);
    });

    app.route('/ifsc-code/:ifscCode').get((req, res) => {
        let options = {
            url: [IFSC_API, req.params.ifscCode].join('/')
        };
        request.get(options).pipe(res);
    });

    app.route('/components/:tenant/:path(*)').get((req, res) => {
        let localPath = app.get('appPath');
        let tenantPath = localPath + req.originalUrl;
        fs.accessAsync(tenantPath).then(() => res.sendFile(tenantPath)).error(() => {
            let lendfoundryPath = localPath + '/components/lendfoundry/' + req.params.path;
            fs.accessAsync(lendfoundryPath).then(() => res.sendFile(lendfoundryPath)).error(() => res.status(404).send("File not found."));
        });
    });

    app.route('/').get((req, res) => {
        res.sendFile('index.html', {
            root: app.get('appPath')
        });
    });
};