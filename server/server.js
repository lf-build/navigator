'use strict';

const PORT = process.env.PORT || 5000;
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const bunyan = require("bunyan");

var RequestLogger = require("./RequestLogger");

function levelStringToStream() {
    return {
        write: log => {

            // Map int log level to string
            const clonedLog = Object.assign({}, log, {
                level: bunyan.nameFromLevel[log.level]
            });

            var logLine = JSON.stringify(clonedLog, bunyan.safeCycles()) + '\n';
            process.stdout.write(logLine);
        }
    };
}

function logger(){
    return bunyan.createLogger({
        name: 'navigator',
        service:'navigator',
        streams: [{
            type: 'raw',
            stream: levelStringToStream()
          }]
    });
}

function requestLogger() {
    return new RequestLogger(logger());
}

var myLogger = function (req, res, next) {
    var log = requestLogger();
    try {
        next()
    }
    catch (error) {
        log.error(req, res, error);
        return;
    }
    log.info(req, res);
}

app.use(myLogger);

require('./express')(app);

require('./routes')(app);


server.listen(PORT, () => logger().info({"message":"Portal is started on port " +  PORT,"properties":{
    configuration:process.env.CONFIGURATION_HOST,
    security:process.env.SECURITY_HOST,
    routingNumber:process.env.ROUTING_NUMBER_API,
    tenant:process.env.APP_TENANT_NAME,
    environment:process.env.NODE_ENV
}}));

module.exports = app;