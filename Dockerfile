FROM registry3.lendfoundry.com/base-navigator:v1.0.4 as builder

WORKDIR /src

ADD package.json /src/package.json
RUN npm install --development

ADD bower.json /src/bower.json
ADD .bowerrc /src/.bowerrc
RUN bower install --allow-root

LABEL RevisionNumber "<<RevisionNumber>>"

ADD ./app /src/app
ADD ./server /src/server
ADD Gruntfile.js /src/Gruntfile.js

RUN grunt --force build

RUN grunt build \
    && grunt service-worker \
    && rm -rf /src/app /src/node_modules \
    && npm install --production \
    && rm -rf /src/dist/bower_components Gruntfile.js Dockerfile docker-compose.yml \
    && rm -rf bower.json package.json package-lock.json README.md launch \
    && rm -rf .gitignore .tmp .git .jshintrc .dockerignore .bowerrc build_script.sh launch.bat yarn.lock commitlint.config.js \
    && cp -rv /src/dist/sw.js /src/

FROM node:9-slim

RUN npm install -g pm2

WORKDIR /home/src

COPY --from=builder /src/. .

ENTRYPOINT \
    sed -i 's~APP_TENANT_NAME~'${TENANT_NAME}'~g' ./dist/index.html && \
    pm2-docker --raw start ./server/server.js