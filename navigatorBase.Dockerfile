FROM node:slim

RUN apt-get update
RUN apt-get install -y git
RUN apt-get install -y git-core
RUN npm install -g grunt-cli 
RUN npm install -g bower 
RUN npm install -g git