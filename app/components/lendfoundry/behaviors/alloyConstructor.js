var Alloy = function(obj) {
    if (obj.behaviors) {
        obj.behaviors = obj.behaviors.filter(function(behavior) {
            if (behavior === commandBus || behavior === eventHub) {
                return false;
            }
            return true;
        })
    } else {
        obj.behaviors = [];
    }
    obj.behaviors.push(commandBus, eventHub);

    return Polymer(obj)
}